import React from 'react';

class App extends React.Component {
    constructor(props) {
        console.log('Constructor',props)
       super(props);
       
       this.state = {
          data: 0,
          msg:''
       }
       this.setNewNumber = this.setNewNumber.bind(this)
     };
    setNewNumber() {
       this.setState({data: this.state.data + 1})
       console.log('data ',this.state.da)
    }
    setNewMsg(newMsg) {
        this.setState({msg: newMsg})
     }
    render() {
       return (
          <div>
             <button onClick = {this.setNewNumber}>INCREMENT</button>
             <Content myNumber = {this.state.data}></Content>
          </div>
       );
    }
 }
 class Content extends React.Component {
     //executes immediately before initial rendering 
    componentWillMount(b) { 
       console.log('Component WILL MOUNT!',b)
    }
    //executes immediately before initial rendering 
    componentDidMount(an) { 
       console.log('Component DID MOUNT!',an)
    }
    //when components receives new props   
    componentWillReceiveProps(newProps) { 
       console.log('Component WILL RECIEVE PROPS!',newProps) 
    }//before rendering after receiving new props or state
    shouldComponentUpdate(newProps, newState) {
        console.log('shouldCcomponentUpdate ',newProps,newState)
        return true;
    }
    //before rendering after receiving new props or state
    componentWillUpdate(nextProps, nextState) {
       console.log('Component WILL UPDATE!',nextProps,nextState);
    }
    //after component's updates are flushed to DOM
    componentDidUpdate(prevProps, prevState) {
       console.log('Component DID UPDATE!',prevProps,prevState)
    }
    componentWillUnmount() {
       console.log('Component WILL UNMOUNT!')
    }
    render() {
       return (
          <div>
             <h3>{this.props.myNumber}</h3>
          </div>
       );
    }
 }
 export default App;